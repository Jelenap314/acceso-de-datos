import com.jelena.gui.Controlador;
import com.jelena.gui.Modelo;
import com.jelena.gui.Vista;

import java.io.IOException;
/**
 * Clase Main donde estoy creando modelo vista controlador
 * estoy pasando vista y modelo a traves de controlador porque estoy sigiendo el modelo vista controlador
 * porque vista y modelo no se conocen entre si
 * @author Jelena
 * @version 1.0
 * @since 23.11.2020.
 */
public class Main {

    public static void main(String[] args) throws IOException {

        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);

    }


}
