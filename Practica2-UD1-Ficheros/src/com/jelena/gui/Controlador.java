package com.jelena.gui;

import com.jelena.base.Libro;
import com.jelena.base.Pelicula;
import com.jelena.base.ProductoDeLibreria;
import com.jelena.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

/**
 * La clase Controlador de mi aplicacion, junta toda la informaicion de mi clase Modelo y Vista
 * Aqui controlo todos mis buttones y implemento listeners
 * tambien tengo los metodos que necesito para manejar con esta clase
 * @author Jelena
 * @version 1.0
 * @since 23.11.2020.
 */
public class Controlador implements ActionListener , ListSelectionListener, WindowListener{
    /**
     * atributos que tiene controlador
     */
    private Vista vista;
    private Modelo modelo;
    private File ultimaRutaExportada;
    /**
     * Constructor donde paso dos objetos vista y modelo
     * @param vista la parte visual de mi aplicacion
     * @param modelo la parte logica de programa
     * contiene tambien todos los listeners y metodo cargaDatos
     */
    public Controlador(Vista vista, Modelo modelo) {

        this.vista = vista;
        this.modelo = modelo;

        try {
            cargaDatosConf();
        } catch (IOException e) {
            e.printStackTrace();
        }

        anadirListeners(this);
        addWindowListener(this);
        addListSelectionListener(this);

    }
    /**
     * metodo para anadir listeners a bottones; asigna los eventos de accion a botoones
     * @param listener para escuchar eventos
     */
    public void anadirListeners(ActionListener listener){

        vista.rbtnLibro.addActionListener(this);
        vista.rbtnPelicula.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnExportar.addActionListener(this);
        vista.btnImportar.addActionListener(this);
        vista.btnEliminar.addActionListener(this);
        vista.btnPrecioMax.addActionListener(this);

    }

    /**
     * Metodo para anadir windowListener
     * @param listener
     */
    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    /**
     * Metodo para anadir ListSelectionListener
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.jList.addListSelectionListener(listener);
    }

    /**
     * Metodo que nos permite asignar el evento a una variable de tipo String.
     * A cada elemento de la interfaz grafica esta asignado un ActionCommand
     * @param e de tipo ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();

        switch(comando){

            case "Nuevo":

                if(siEsVacio()){

                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "Titulo\nPrecio\nGenero\nFecha" +
                            vista.escritorDirector.getText());

                    break;
                }


                if (vista.rbtnLibro.isSelected()) {
                    modelo.altaLibro(vista.txtTitulo.getText(), Integer.parseInt(vista.txtPrecio.getText()), vista.txtGenero.getText(),
                            vista.dpFechaCompra.getDate(), vista.txtEscritorDirector.getText());

                } else {
                    modelo.altaPelicula(vista.txtTitulo.getText(), Integer.parseInt(vista.txtPrecio.getText()), vista.txtGenero.getText(),
                            vista.dpFechaCompra.getDate(), vista.txtEscritorDirector.getText());
                }

                break;

            case "PrecioMax":

                ProductoDeLibreria productoMax = new ProductoDeLibreria();

               productoMax = modelo.ordenarPorprecio();

               vista.precioMax.setText("El producto mas caro es " + productoMax.getTitulo() + " y su precio es " + productoMax.getPrecio());

                break;

            case "Eliminar":

                modelo.eliminarLibroPelicula(vista.txtTitulo.getText());
                refrescarLista();

                break;

            case "Exportar":
                JFileChooser seleccionarFichero1 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = seleccionarFichero1.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(seleccionarFichero1.getSelectedFile());
                        updateDatosConf(seleccionarFichero1.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;

            case "Importar":
                JFileChooser seleccionarFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = seleccionarFichero2.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(seleccionarFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescarLista();
                }
                break;

            case "Libro":
                vista.escritorDirector.setText("Nombre Escritor");
                break;
            case "Pelicula":
                vista.escritorDirector.setText("Nombre Director");
                break;

        }

        refrescarLista();
        limpiar();

    }

    /**
     * este metodo sirve si textField es vacio que nos devuelve boolean falso
     * @return
     */
    private boolean siEsVacio() {
        if (vista.txtTitulo.getText().isEmpty() ||
                vista.txtPrecio.getText().isEmpty() ||
                vista.txtGenero.getText().isEmpty() ||
                vista.txtEscritorDirector.getText().isEmpty() ||
                vista.dpFechaCompra.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Metodo para refrescar lista despues cambios
     */
    private void refrescarLista() {
        vista.dlmLibreria.clear();
        for (ProductoDeLibreria producto : modelo.getListaProductos()) {
            vista.dlmLibreria.addElement(producto);
        }
    }

    /**
     * este metodo sirve para dejar textField en blanco despues usarlo
     */
    private void limpiar() {
        vista.txtTitulo.setText(null);
        vista.txtPrecio.setText(null);
        vista.txtGenero.setText(null);
        vista.txtEscritorDirector.setText(null);
        vista.dpFechaCompra.setText(null);

    }

    /**
     * Metodo que carga datos de fichero config, tambien tiene una excepxion IO
     * @throws IOException excepcion tipo IO
     */
    private void cargaDatosConf() throws IOException {
        Properties config = new Properties();
        config.load(new FileReader("libreria.conf"));
        ultimaRutaExportada = new File(config.getProperty("ultimaRutaExportada"));
    }

    /**
     * Metodo que actualiza los datos config
     * @param ultimaRutaExportada de tipo file
     */
    private void updateDatosConf(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * metodo que guarda los datos de fichero config
     * @throws IOException
     */
    private void guardarConfig() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("libreria.conf"), "Datos configuracion libreria");

    }

    /**
     * metodo sobrescrito de la clase WindowEvent
     * @param e de tipo WindowEvent
     */
    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * metodo sobrescrito de WindowEvent
     * @param e de tipo WindowEvent
     */
    @Override
    public void windowClosing(WindowEvent e) {

        int seleccionado = Util.mensajeConfirmacion("Quieres salir de la ventana?", "EXIT");
        if (seleccionado == JOptionPane.YES_OPTION) {
            try {
                guardarConfig();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            System.exit(0);
        }

    }

    /**
     * metodo sobrescrito de WindowEvent
     * @param e de tipo WindowEvent
     */
    @Override
    public void windowClosed(WindowEvent e) {

    }
    /**
     * metodo sobrescrito de WindowEvent
     * @param e de tipo WindowEvent
     */
    @Override
    public void windowIconified(WindowEvent e) {

    }
    /**
     * metodo sobrescrito de WindowEvent
     * @param e de tipo WindowEvent
     */
    @Override
    public void windowDeiconified(WindowEvent e) {

    }
    /**
     * metodo sobrescrito de WindowEvent
     * @param e de tipo WindowEvent
     */
    @Override
    public void windowActivated(WindowEvent e) {

    }
    /**
     * metodo sobrescrito de WindowEvent
     * @param e de tipo WindowEvent
     */
    @Override
    public void windowDeactivated(WindowEvent e) {

    }
    /**
     * metodo sobrescrito de WindowEvent
     * @param e de tipo SelectionEvent
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()) {
            ProductoDeLibreria producto = (ProductoDeLibreria) vista.jList.getSelectedValue();
            vista.txtTitulo.setText((producto.getTitulo()));
            vista.txtPrecio.setText(String.valueOf((producto.getPrecio())));
            vista.txtGenero.setText(producto.getGenero());
            vista.dpFechaCompra.setDate(producto.getFechaCompra());

            if (producto instanceof Libro) {
                vista.rbtnLibro.doClick();
                vista.txtEscritorDirector.setText(((Libro) producto).getNombreEscritor());
            } else {
                vista.rbtnPelicula.doClick();
                vista.txtEscritorDirector.setText(((Pelicula) producto).getNombreDirector());
            }
        }

    }
}
