package com.jelena.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.jelena.base.ProductoDeLibreria;

import javax.swing.*;

public class Vista {
    private JPanel panel1;
    JFrame frame;
    JRadioButton rbtnLibro;
    JRadioButton rbtnPelicula;
    JTextField txtTitulo;
    JTextField txtPrecio;
    JTextField txtGenero;
    JTextField txtnombreEscritor;
    JButton btnNuevo;
    JButton btnExportar;
    JButton btnImportar;
    DatePicker dpFechaCompra;
    JList jList;
    JLabel escritorDirector;
    JTextField txtEscritorDirector;
    JButton btnEliminar;
    JButton btnPrecioMax;
    JLabel precioMax;

    DefaultListModel<ProductoDeLibreria> dlmLibreria;

    public Vista() {

        frame = new JFrame("Libreria");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(600, 400);
        frame.setVisible(true);

        frame.setLocationRelativeTo(null);

        iniciarajlist();

    }

    private void iniciarajlist() {
        dlmLibreria = new DefaultListModel<ProductoDeLibreria>();
        jList.setModel(dlmLibreria);
    }


}

