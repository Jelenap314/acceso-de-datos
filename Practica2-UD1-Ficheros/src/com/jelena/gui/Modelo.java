package com.jelena.gui;

import com.jelena.base.Libro;
import com.jelena.base.Pelicula;
import com.jelena.base.ProductoDeLibreria;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
/**
 * La clase modelo  cual contiene toda la logica de mi aplicacion.
 * Contiene LinkedList, tambien todos los metodos y operaciones necesarios para el correcto
 * funcionamiento de aplicacion
 * @author Jelena
 * @version 1.0
 * @since 23.11.2020.
 */
public class Modelo {
    /**
     * atributos que contiene la clase modelo
     */
    private ArrayList<ProductoDeLibreria> listaProductos;
    /**
     * el constructor de clase modelo que inicializa ArrayList
     */
    public Modelo(){

        this.listaProductos = new ArrayList<>();

    }

    /**
     * metodo que esta dando alta de los libros y esta pasando los atributos de clase Libro
     * @param titulo parametro titulo
     * @param precio parametro precio
     * @param genero parametro genero
     * @param fechaCompra parametro fecha compra
     * @param nombreEscritor parametro nombre escritor
     */
    public void altaLibro(String titulo, int precio, String genero, LocalDate fechaCompra, String nombreEscritor){

        listaProductos.add(new Libro(titulo, precio, genero, fechaCompra, nombreEscritor));

    }
    /**
     * metodo que esta dando alta de los Peliculas y esta pasando los atributos de clase Peliculas
     * @param titulo parametro titulo
     * @param precio parametro precio
     * @param genero parametro genero
     * @param fechaCompra parametro fecha compra
     * @param nombreDirector parametro nombre director
     */
    public void altaPelicula(String titulo, int precio, String genero, LocalDate fechaCompra, String nombreDirector){

        listaProductos.add(new Pelicula(titulo, precio, genero, fechaCompra, nombreDirector));

    }
    /**
     * Metodo que elimina los libros  o peliculas
     * @param titulo parametro titulo
     */
    public void eliminarLibroPelicula(String titulo) {

        int position = 0;

        for (ProductoDeLibreria producto : listaProductos) {

            if (producto.getTitulo().equalsIgnoreCase(titulo)) {

                position = listaProductos.indexOf(producto);

            }
        }

        listaProductos.remove(position);
    }

    /**
     * metodo que elige precio maximo
     * @return devuelve el precio mas caro
     */
    public ProductoDeLibreria ordenarPorprecio(){

        ProductoDeLibreria masCaro = new ProductoDeLibreria();

        for(ProductoDeLibreria producto : listaProductos){

            if(producto.getPrecio()> masCaro.getPrecio()){

                masCaro = producto;

            }

            }

        return masCaro;

    }

    /**
     * metodo que exporta fichero xml
     * @param file de clase File
     * @throws ParserConfigurationException excepcion
     * @throws TransformerException excepcion
     */
    public void exportarXML(File file) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document document = dom.createDocument(null, "xml", null);

        Element root = document.createElement("ProductosDeLibreria");
        document.getDocumentElement().appendChild(root);

        Element nodoLibreria = null, nodoDatos = null;
        Text text = null;

        for (ProductoDeLibreria producto : listaProductos) {

            if (producto instanceof Libro) {
                nodoLibreria = document.createElement("Libro");

            } else {
                nodoLibreria = document.createElement("Pelicula");
            }
            root.appendChild(nodoLibreria);

            nodoDatos = document.createElement("titulo");
            nodoLibreria.appendChild(nodoDatos);

            text = document.createTextNode(producto.getTitulo());
            nodoDatos.appendChild(text);

            nodoDatos = document.createElement("precio");
            nodoLibreria.appendChild(nodoDatos);

            text = document.createTextNode(String.valueOf(producto.getPrecio()));
            nodoDatos.appendChild(text);

            nodoDatos = document.createElement("genero");
            nodoLibreria.appendChild(nodoDatos);

            text = document.createTextNode(producto.getGenero());
            nodoDatos.appendChild(text);

            nodoDatos = document.createElement("fecha-compra");
            nodoLibreria.appendChild(nodoDatos);

            text = document.createTextNode(producto.getFechaCompra().toString());
            nodoDatos.appendChild(text);

            if (producto instanceof Libro) {
                nodoDatos = document.createElement("nombre-escritor");
                nodoLibreria.appendChild(nodoDatos);
                text = document.createTextNode(((Libro) producto).getNombreEscritor());
                nodoDatos.appendChild(text);
            } else {
                nodoDatos = document.createElement("kms");
                nodoLibreria.appendChild(nodoDatos);
                text = document.createTextNode(((Pelicula) producto).getNombreDirector());
                nodoDatos.appendChild(text);
            }

        }

        Source source = new DOMSource(document);
        Result res = new StreamResult(file);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, res);
    }

    /**
     * metodo que improta fichero xml
     * @param file de la clase File
     * @throws ParserConfigurationException excepcion
     * @throws IOException excepcion
     * @throws SAXException excepcion
     */
    public void importarXML(File file) throws ParserConfigurationException, IOException, SAXException {
        listaProductos = new ArrayList<ProductoDeLibreria>();
        Libro libro = null;
        Pelicula pelicula = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);

        NodeList nodeList = document.getElementsByTagName("*");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Element nodoLibreria = (Element) nodeList.item(i);


            if (nodoLibreria.getTagName().equals("Libro")) {
                libro = new Libro();
                libro.setTitulo(nodoLibreria.getChildNodes().item(0).getTextContent());
                libro.setPrecio(Integer.parseInt(nodoLibreria.getChildNodes().item(1).getTextContent()));
                libro.setGenero(nodoLibreria.getChildNodes().item(2).getTextContent());
                libro.setFechaCompra(LocalDate.parse(nodoLibreria.getChildNodes().item(3).getTextContent()));
                libro.setNombreEscritor(nodoLibreria.getChildNodes().item(4).getTextContent());

                listaProductos.add(libro);
            } else {
                if (nodoLibreria.getTagName().equals("Pelicula")) {
                    pelicula = new Pelicula();
                    pelicula.setTitulo(nodoLibreria.getChildNodes().item(0).getTextContent());
                    pelicula.setPrecio(Integer.parseInt(nodoLibreria.getChildNodes().item(1).getTextContent()));
                    pelicula.setGenero(nodoLibreria.getChildNodes().item(2).getTextContent());
                    pelicula.setFechaCompra(LocalDate.parse(nodoLibreria.getChildNodes().item(3).getTextContent()));
                    pelicula.setNombreDirector(nodoLibreria.getChildNodes().item(4).getTextContent());

                    listaProductos.add(pelicula);
                }
            }


        }
    }

    /**
     * @return devuelve el ArrayList
     */
    public ArrayList<ProductoDeLibreria> getListaProductos() {
        return listaProductos;
    }
}
