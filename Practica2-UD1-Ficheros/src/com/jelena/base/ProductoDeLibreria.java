package com.jelena.base;

import java.time.LocalDate;
/**
 *La clase Producto de Libreria contiene todas las informaciones y metodos sobre el productos de libreria
 * @author Jelena
 * @version 1.0
 * @since 23.11.2020.
 */
public class ProductoDeLibreria {
    /**
     * atributos que necesito para mi clase
     */
    private String titulo;
    private int precio;
    private String genero;
    private LocalDate fechaCompra;
    /**
     *Constructor donde paso cuatro objetos
     * @param titulo parametro titulo
     * @param precio parametro precio
     * @param genero parametro genero
     * @param fechaCompra parametro fechaCompra
     */
    public ProductoDeLibreria(String titulo, int precio, String genero, LocalDate fechaCompra) {
        this.titulo = titulo;
        this.precio = precio;
        this.genero = genero;
        this.fechaCompra = fechaCompra;
    }

    /**
     * Constructor vacio
     */
    public ProductoDeLibreria(){

    }

    /**
     * @return devuelve titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     *
     * @param titulo nos permita fijar titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return devuelve precio
     */
    public int getPrecio() {
        return precio;
    }

    /**
     *
     * @param precio nos permita fijar precio
     */
    public void setPrecio(int precio) {
        this.precio = precio;
    }

    /**
     * @return devuelve genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     *
     * @param genero nos permita fijar genero
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * @return devuelve fecha compra
     */
    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    /**
     *
     * @param fechaCompra nos permita fijar fecha compra
     */
    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    /**
     *
     * @return devuelve todos los parametros asociados a la clase Libro
     */
    @Override
    public String toString() {
        return titulo + "-" + precio + "-" + genero;
    }

}