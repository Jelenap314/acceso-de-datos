package com.jelena.base;

import java.time.LocalDate;
/**
 *La clase Libro contiene todas las informaciones y metodos sobre el objeto Libro esta heredando de la clase Productos de Libreria
 * @author Jelena
 * @version 1.0
 * @since 23.11.2020.
 */
public class Libro extends ProductoDeLibreria{
    /**
     * atributos de la clase Libro
     */
    private String nombreEscritor;

    /**
     * Constructor vacio
     */
    public Libro(){

    }

    /**
     *Constructor donde paso cinco objetos
     * @param titulo parametro titulo
     * @param precio parametro precio
     * @param genero parametro genero
     * @param fechaCompra parametro fechaCompra
     * @param nombreEscritor parametro nombreEscritor
     */
    public Libro(String titulo, int precio, String genero, LocalDate fechaCompra, String nombreEscritor) {
        super(titulo, precio, genero, fechaCompra);
        this.nombreEscritor = nombreEscritor;
    }

    /**
     *
     * @return devuelve nombre de escritor
     */
    public String getNombreEscritor() {
        return nombreEscritor;
    }

    /**
     *
     * @param nombreEcritor nos permita fijar nombre de escritor
     */
    public void setNombreEscritor(String nombreEcritor) {
        this.nombreEscritor = nombreEcritor;
    }

    /**
     *
     * @return todos los parametros asociados a la clase Libro
     */
    @Override
    public String toString() {
        return "Libro: " + getTitulo() + "-" + getPrecio() + "-" + getGenero() + "-" + nombreEscritor;
    }
}
