package com.jelena.base;

import java.time.LocalDate;
/**
 *La clase Pelicula contiene todas las informaciones y metodos sobre el objeto Pelicula y esta heredando de la clase Productos de Libreria
 * @author Jelena
 * @version 1.0
 * @since 23.11.2020.
 */
public class Pelicula extends ProductoDeLibreria{
    /**
     * atributos que tiene controlador
     */
    private String nombreDirector;

    /**
     * constructor vacio
     */
    public Pelicula() {
        super();
    }
    /**
     *Constructor donde paso cinco objetos
     * @param titulo parametro titulo
     * @param precio parametro precio
     * @param genero parametro genero
     * @param fechaCompra parametro fechaCompra
     * @param nombreDirector parametro nombreEscritor
     */
    public Pelicula(String titulo, int precio, String genero, LocalDate fechaCompra, String nombreDirector) {
        super(titulo, precio, genero, fechaCompra);
        this.nombreDirector = nombreDirector;
    }

    /**
     * @return devuelve nombre de director
     */
    public String getNombreDirector() {
        return nombreDirector;
    }

    /**
     *
     * @param nombreDirector nos permita fijar nombre de director
     */
    public void setNombreDirector(String nombreDirector) {
        this.nombreDirector = nombreDirector;
    }

    /**
     * @return todos los parametros asociados a la clase Libro
     */
    @Override
    public String toString() {
        return "Pelicula: " + getTitulo() + "-" + getPrecio() + "-" + getGenero()+ "-" + nombreDirector;
    }
}
