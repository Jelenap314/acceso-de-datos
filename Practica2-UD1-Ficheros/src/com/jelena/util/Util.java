package com.jelena.util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

/**
 * La clase util que maneja mensajes de dialogo de JOptionPane
 *  @author Jelena
 *  @version 1.0
 *  @since 23.11.2020.
 *
 */
public class Util {
    /**
     * Metodo que crea mensaje Error
     * @param mensaje parametro mensaje
     */
    public static void mensajeError(String mensaje) {

        JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Metodo que crea mensaje si quieremos salir de aplicavion o no
     * @param mensaje parametro mensaje
     * @param titulo parametro titulo
     * @return devuelve mensaje de  JOptionPane
     */
    public static int mensajeConfirmacion(String mensaje, String titulo) {

        return JOptionPane.showConfirmDialog(null,mensaje,titulo,JOptionPane.YES_NO_OPTION);
    }

    /**
     * metodo que crea selector de ficheros
     * @param rutaDefecto de la clase File
     * @param tipoArchivos parametro tipo archivos
     * @param extension parametro exrension
     * @return devuelve selectorFichero
     */
    public static JFileChooser crearSelectorFicheros(File rutaDefecto, String tipoArchivos, String extension) {

        JFileChooser selectorFichero = new JFileChooser();

        if (rutaDefecto!=null) {
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if (extension!=null) {
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivos,extension);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }

}
