package com.jelena.base;

/**
 *L clase Libro contiene todas las informacione sobre el objeto Libro
 */
public class Libro {
    /**
     * atributos de la clase Libro
     */
    private String isbn;
    private String titulo;

    /**
     * Constructor donde paso dos objetos
     * @param isbn parametro isbn
     * @param titulo parametro titulo
     */
    public Libro(String isbn, String titulo) {

        this.isbn = isbn;
        this.titulo = titulo;

    }

    /**
     * Constructor vacio
     */
    public Libro() {
    }

    /**
     * @return metodo que devuelve titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo nos permita fijar titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     *
     * @return metodo que devuelve isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     *
     * @param isbn nos permita fijr titulo
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     *
     * @return todos los parametros asociados a la clase Libro
     */
    @Override
    public String toString() {
        return isbn + " - " + titulo;
    }


}
