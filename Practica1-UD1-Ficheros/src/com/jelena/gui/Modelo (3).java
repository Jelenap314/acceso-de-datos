package com.jelena.gui;

import com.jelena.base.Libro;

import java.util.LinkedList;

/**
 * La clase modelo  cual contiene toda la logica de mi aplicacion.
 * Contiene LinkedList, tambien todos los metodos y operaciones necesarios para el correcto
 * funcionamiento de aplicacion
 * @author Jelena
 * @version 1.0
 * @since 13.11.2020.
 */
public class Modelo {
    /**
     * atributos que contiene la clase modelo
     */
    private LinkedList<Libro> lista;

    /**
     * el constructor de clase modelo que inicializa el LinkedList
     */
    public Modelo() {
        this.lista = new LinkedList<>();

    }


    /**
     * metodo que esta dando alta de los libros y esta pasando los atributos de clase Libro
     * @param isbn parametro isbn
     * @param titulo parametro isbn
     */
    public void altaLibro(String isbn, String titulo){

        lista.add(new Libro(isbn, titulo));

    }

    /**
     * Metodo que elimina los libros seleccionados
     * @param libro objeto Libro
     */
    public void eliminarLibro(Libro libro){

        lista.remove(libro);

    }

    /**
     * metodo que nos permite modificar titulos de libros
     * @param isbnModificar para modificar isbn
     * @param nuevoTitulo para modificar titulo
     */
    public void actualizar(String isbnModificar, String nuevoTitulo) {

        for(Libro libro : lista){

            if(libro.getIsbn().equalsIgnoreCase(isbnModificar)){

                libro.setTitulo(nuevoTitulo);

            }

        }

    }

    /**
     * @return metodo que esta devolviendo LinkedList
     */
    public LinkedList<Libro> getLista() {
        return lista;
    }

    /**
     * @param lista de LinkedList nos permitira fijar LinkedList
     */
    public void setLista(LinkedList<Libro> lista) {
        this.lista = lista;
    }

    /**
     *
     * @return todos los parametros asociados a la clase modelo
     */
    @Override
    public String toString() {
        return "Programa{" +
                "lista=" + lista +
                '}';
    }


}
