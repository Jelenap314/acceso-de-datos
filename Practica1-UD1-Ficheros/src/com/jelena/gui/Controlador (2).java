package com.jelena.gui;

import com.jelena.base.Libro;
import org.w3c.dom.*;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

/**
 * La clase Controlador de mi aplicacion, junta toda la informaicion de mi clase Modelo, Vista y Login
 * Aqui controlo todos mis buttones a a lo que agregué ActionListener
 * tambien tengo los metodos que necesito para manejar con esta clase
 * @author Jelena
 * @version 1.0
 * @since 13.11.2020.
 */
public class Controlador implements ActionListener {
    /**
     * atributos que tiene controlador
     */
    private Vista vista;
    private Modelo modelo;

    /**
     * Constructor donde paso dos objetos vista y modelo
     * @param vista la parte visual de mi aplicacion
     * @param modelo la parte logica de programa
     * contiene tambien el metodo anadir listeners wue asigna los listeners a bottones
     */
    public Controlador(Vista vista, Modelo modelo) {

        this.vista = vista;
        this.modelo = modelo;

        anadirListeners(this);

    }

    /**
     * metodo para anadir listeners a bottones; asigna los eventos de accion a botoones
     * @param listener para escuchar eventos
     */
    public void anadirListeners(ActionListener listener){

        vista.btnAlta.addActionListener(this);
        vista.btnMuestra.addActionListener(this);
        vista.itemExportarXML.addActionListener(this);
        vista.itemImportarXML.addActionListener(this);
        vista.btnEliminar.addActionListener(this);
        vista.btnModificar.addActionListener(this);

    }

    /**
     * Metodo que nos permite asignar el evento a una variable de tipo String.
     * A cada elemento de la interfaz grafica esta asignado un ActionCommand
     * @param e de tipo ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();

        switch(comando){

            case "Alta":
            modelo.altaLibro(vista.txtIsbn.getText(), vista.txtTitulo.getText());
            refrescarComboBox();

                break;
            case "Mostrar":
                Libro seleccionado = (Libro) vista.dbm.getSelectedItem();
                vista.txtIsbn.setText(seleccionado.getIsbn());
                vista.txtTitulo.setText(seleccionado.getTitulo());

                break;

            case "Exportar":
                JFileChooser selectFile = new JFileChooser();
                int opcionSeleccionada = selectFile.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                    File file = selectFile.getSelectedFile();
                    exportarXML(file);
                }

                break;

            case "Importar":
                JFileChooser selectFileImportar = new JFileChooser();
                int opcion = selectFileImportar.showOpenDialog(null);
                if (opcion == JFileChooser.APPROVE_OPTION) {
                    File file = selectFileImportar.getSelectedFile();
                    importarXML(file);
                    refrescarComboBox();
                }

                break;

            case "Eliminar":

                Libro selected = (Libro) vista.dbm.getSelectedItem();
                modelo.eliminarLibro(selected);
                refrescarComboBox();

                break;

            case "Modificar":

              String isbnModificar = vista.txtIsbn.getText();
              String nuevoTitulo = vista.txtTitulo.getText();
              modelo.actualizar(isbnModificar, nuevoTitulo);

              refrescarComboBox();

                break;

        }

    }

    /**
     * este metodo sirve para dejar textField en blanco despues de usarlo
     */
    private void dejarBlanco(){

        vista.txtIsbn.setText("");
        vista.txtTitulo.setText("");

    }

    /**
     * metodo refrescarCombobox sirve para refrescar comboBox despues dar la alta, modificar, importar o eliminar los libros
     * contiene el metodo dejarBlanco para dejar en blanco los textFields despues de usar los
     */
    private void refrescarComboBox() {

      vista.dbm.removeAllElements();

      for(Libro libro : modelo.getLista()){

         vista.dbm.addElement(libro);

      }

      dejarBlanco();

    }

    /**
     * metodo que exporta los datos de aplicacion a xml
     * @param file de tipo File
     */
    private void exportarXML(File file) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            Document document = dom.createDocument(null, "xml", null);

            Element root = document.createElement("libros");
            document.getDocumentElement().appendChild(root);

            Element nodeLibro;
            Element nodeDatas;
            Text data;

            for (Libro libro : modelo.getLista()) {

                nodeLibro = document.createElement("libro");
                root.appendChild(nodeLibro);

                nodeDatas = document.createElement("isbn");
                nodeLibro.appendChild(nodeDatas);

                data = document.createTextNode(libro.getIsbn());
                nodeDatas.appendChild(data);

                nodeDatas = document.createElement("titulo");
                nodeLibro.appendChild(nodeDatas);

                data = document.createTextNode(libro.getTitulo());
                nodeDatas.appendChild(data);
            }

            Source src = new DOMSource(document);
            Result result = new StreamResult(file);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo que importa los xml datos de aplicaion
     * @param file de tipo File
     */
    private void importarXML(File file) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(file);

            NodeList libros = document.getElementsByTagName("libro");
            for (int i = 0; i < libros.getLength(); i++) {
                Node libro = libros.item(i);
                Element element = (Element) libro;

                String isbn = element.getElementsByTagName("isbn").item(0).getChildNodes().item(0).getNodeValue();
                String titulo = element.getElementsByTagName("titulo").item(0).getChildNodes().item(0).getNodeValue();

                modelo.altaLibro(isbn, titulo);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }
    }


}
