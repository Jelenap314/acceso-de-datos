package com.jelena.gui;

import com.jelena.base.Libro;
import com.sun.corba.se.impl.ior.IORTemplateImpl;
import jdk.internal.org.xml.sax.SAXException;
import org.w3c.dom.*;

import javax.swing.*;

/**
 * La clase Vista de mi aplicacion, contiene todos los elementos visuales de la aplicacion
 * Hereda de JFrame
 * @author Jelena
 * @version 1.0
 * @since 13.11.2020
 */
public class Vista extends JFrame{

    /**
     * atributos de la clase vista
     */
    private JPanel panel1;
    JTextField txtIsbn;
    JTextField txtTitulo;
    JComboBox comboBox;
    JButton btnAlta;
    JButton btnMuestra;
    JButton btnEliminar;
    JButton btnModificar;
    JLabel lblLibro;
    JMenuItem itemExportarXML;
    JMenuItem itemImportarXML;
    DefaultComboBoxModel <Libro> dbm;


    /**
     * inicializacion de constructor
     */
    public Vista() {

        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setSize(600, 400);
        setVisible(true);

        menu();

        setLocationRelativeTo(null);
        dbm = new DefaultComboBoxModel<>();
        comboBox.setModel(dbm);


        }

    /**
     * para crear el menu desplegable de la aplicacion
     * contendra la opcion de exportar/importar el fichero xml
     */
    private void menu() {
        JMenuBar bar = new JMenuBar();
        JMenu menu = new JMenu("Ficheros");
        itemExportarXML = new JMenuItem("Exportar XML");
        itemImportarXML = new JMenuItem("Importar XML");

        itemExportarXML.setActionCommand("Exportar");
        itemImportarXML.setActionCommand("Importar");

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);

        bar.add(menu);
        setJMenuBar(bar);
    }


}





